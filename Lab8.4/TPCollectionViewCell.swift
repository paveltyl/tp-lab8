//
//  TPCollectionViewCell.swift
//  Lab8.4
//
//  Created by Pavel Tyletsky on 16/05/2017.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

import UIKit

class TPCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var author_and_title: UILabel!
    @IBOutlet weak var image: UIImageView!
}
