//
//  BookViewController.swift
//  Lab8.4
//
//  Created by Pavel Tyletsky on 16/05/2017.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

import UIKit

class BookViewController: UIViewController {

    @IBOutlet weak var authorView: UILabel!
    @IBOutlet weak var descripView: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    var image:UIImage? = nil
    var author:String? = nil
    var descrip: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = image
        authorView.text = author
        descripView.text = descrip
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
