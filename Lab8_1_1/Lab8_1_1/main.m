//
//  main.m
//  Lab8_1_1
//
//  Created by Pavel Tyletsky on 16/05/2017.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
