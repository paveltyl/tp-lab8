//
//  Record.h
//  Lab8_1_1
//
//  Created by Pavel Tyletsky on 16/05/2017.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Record : NSManagedObject

@property (nonatomic, retain) NSString * aviaCompany;
@property (nonatomic, retain) NSString * cityFrom;
@property (nonatomic, retain) NSString * cityTo;
@property (nonatomic, retain) NSNumber * price;

@end
